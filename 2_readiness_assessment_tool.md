---
document set: DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Digital Security Readiness Assessment Tool
author: Jonah Silas Sheridan, Lisa Jervis for Information Ecology
last modified: 11/27/17
version: "2.02, PEER REVIEWED with note"
---
### *A standalone version of this tool has been created and renamed as the [Organizational Security Assessment Tool](https://ecl.gy/orgsec-assess). Though not peer reviewed the Organizational Security Assessment Tool is more up to date as of 4/2022 and includes a section on [Extended Work from Home Security](https://0xacab.org/iecology/orgsec-assessment/-/blob/master/orgsec_assessment.md#extended-work-from-home-security-posture) specifically tuned for the era of coronavirus.*

# Digital Security Readiness Assessment Tool

## Introduction

This assessment tool is meant to help organizations identify where their most critical security needs lie. Many common information systems and technology practices are oriented around providing or supporting security outcomes. Because of that, organizations that have foundational technology capacity issues are best served by putting energy improving baseline systems before taking on new security initiatives.

The tool is laid out as a number of items to assess broken out across three categories: cultural hallmarks of security success, information technology operations that support security outcomes, and digital security baseline capacities. Go through each section one by one.

For each item you, should grade your organization honestly on a scale from 1 to 10. At the end of each category, there is space to put a subtotal for that section.

After you have completed all three sections, add up the three subtotals to get your total score.

If you have a total score of 75, no section under 25 and no single item under 5 you should feel confident undertaking the rest of these checklists. Otherwise, your organization should concentrate first on any areas where you have very low scores, and overall on building capacity in these foundational areas before pursuing additional digital security improvements.

Even if you are at or above the thresholds indicated, be sure to note the places you have low individual or section scores and talk with your leadership and technology-responsible staff to make plans to improve them as soon as possible. Not consistently addressing and maintaining these foundational capacities will likely undermine your security efforts over time.

## Cultural Hallmarks for Security Success

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have a culture of training and learning, including strong technology training and follow up as part of new staff orientation procedures.**  
*New tools and practices demand end-user training. If your organization doesn't have established practices around training--when new people are hired, when refresher trainings are needed, and when important processes change--implementing improved and possibly complex secure practices is nearly impossible. Beginning with documentation and training for new hires is a wise first step in this area. Following up with new employees at 30-day intervals will ensure they continue to get the support they need to do their work effectively and securely. When a new process is introduced, it is like everyone in your organization is new to it, so initial training with similar follow-up is recommended.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have a common and clearly communicated set of information systems that are administered by the organization and used with defined processes; ensure that all staff follow these processes effectively and are not using other systems for their work.**  
*If your staff are using personal file-sharing, email, task management, or other accounts without knowledge or guidance from the organization, not only will your efficiency suffer but the environment becomes impractical to secure. How can you protect things you have no access to at an administrative level or, worse yet, don't even know are in use? A good place to start figuring this out if by making an inventory, collaboratively with all staff, of all the places that your information is currently stored.*

*An important way this issue shows up in your organization is the use of cloud services. While many organizations use their personal accounts on those systems, official organizational accounts are vastly preferable. If your organization is a registered US 501c3 non-profit, most cloud providers offer licenses for their applications for free or at a discount, providing you significant capacity to centrally manage, back up, and monitor your information at a low cost.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have technology champions at all levels of the organization, especially leadership, and strong supervisory support and participation in systems adoption.**  
*Leadership for technology and operations within your organization can and should come from all levels. Junior staff and younger "digital natives" on staff often use or are open to using more technology in their work so can be motivated to participate in the planning and deployment of information systems and promote uptake among peers. Of course, demonstrations of support for and engagement with technology initiatives from management are also powerful motivators for staff. Visible participation by executive leadership in training on and use of official organizational tools is a powerful modeling of preferred behavior and critical to changing organizational habits and culture.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have a complete policy set describing employees' responsibilities and limitations on their facilities, hardware, and information systems use.**  
*Legal and operating risk due to inconsistent expectations and behavior can hamper even the most well-designed security plan. Managing your risk, employee awareness, and compliance through a strong set of workplace policies around technology but also more generally will set you up for security initiative success.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Develop and evaluate baseline non-technical security practices in an ongoing way.**  
*If you do not control your office space and access to your computers, your other digital security steps can be easily circumvented by walking into your office. Rotate alarm system codes, door codes, wireless network passwords, and other access mechanisms (for example, emergency building access plans) when staff leave the organization. Sophisticated attackers can gain full control of a computer or network with even a short period of physical access to your space or digital access to unsecured systems. More importantly, non-technical security practices help build healthy habits and a culture of security in your organization.*

Subtotal, Cultural Hallmarks: ____

## Information Technology Operations that Support Security Outcomes

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have a recurrent line item for technology in your budget.**  
*Security is an ongoing process and will require regular investments in computer equipment, software, support, and training to be effective. Work with your technical support provider to determine an appropriate amount to put into this line item.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have regular and adequate technical support provided either by staff assigned via job description or contracted with outside agencies.**  
*If your existing hardware and software are not well supported, introducing new tools and practices will likely meet with significant barriers, as new technologies and tools often demand significant ongoing technical support for proper setup and functioning. Your tech support providers are central to your ability to identify and protect your systems from attack, work they can't do if they don't exist. There are as many ways to obtain technical support as there are organizations. Talking to peer organizations in your area is a good way to find quality help.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Regardless of technical support solution, have someone on staff assigned via job description to be responsible for technical operations, including managing technical support providers and systems upgrades.**  
*No matter how you meet your technical support needs, someone needs to have time and responsibility to manage the flow of ongoing support requests, to act as a point person for vendors and consultants, and to lead projects to improve infrastructure. Although this is critical when sourcing technical support services from outside of staff to ensure your organization is owning its own operations, it is perhaps even more important when assigning technical support responsibilities to someone on staff. If internal tech support doesn't have explicit time to put into systems changes and vendor management and can only spend time fixing broken hardware and software systems, your digital security initiatives will suffer from a lack of attention.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Provide relatively new and adequately powered computers to all staff.**  
*Industry standard best practice is to replace laptops and desktops every 3 to 5 years. Encryption tools use a lot of power and can bring older, inadequately powered computers to a near halt, making some security steps untenable for staff. Money for replacing 1/3 to 1/5 of your computers each year should be part of your recurring technology budgeting.*

Subtotal, Technology Operations: ____

## Digital Security Baseline Capacities

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have a process for properly onboarding and offboarding staff and volunteers that includes attention to your information systems.**  
*The expansion or contraction of your team is a critical change in your security context, and so is an important moment to institute strong security measures. Your onboarding process should include detailed steps for the creation of accounts and instructions on how to determine and grant the correct and minimum permissions needed for that person's role. When a staff member or volunteer departs, ensure that any of the organization's data that is on their personal or work devices is copied to relevant organizational systems and/or destroyed as necessary. Also at offboarding, all individual accounts belonging to the outgoing person should be deleted and any organizational passwords that they used or accessed in their work should be changed to something new.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Make sure the computers and other devices you use, including personal devices that staff may use to access organizational information, are only running only the software expected, and only the most recent version of those programs. Have a plan to detect and remove malware, viruses, or other intrusive software and run update tools regularly.**  
*As a digital security first step, ensure you are running antivirus software on all computers. Antivirus software for Macs and Windows computers is available to non-profits at a discounted rate through [Tech Soup](http://techsoup.org) (http://techsoup.org). If you haven't been running antivirus software or otherwise aren't sure about the status of your devices, you can have the operating system (OS) on them reinstalled to help guarantee the computers are free of malware and viruses. This is one benefit of adopting Internet-based tools for your organization's information, in that your data is readily available on a freshly installed system.*

*When reinstalling, use a copy from the OS provider wherever possible. Computer manufacturers often bundle other software in their installs, which may impact privacy and security (so you don't want them) but may also contain specific tools for the hardware, especially in laptops (so you may need them). Immediately after installation of the operating system and common software tools, run software updates for both the operating system and, where needed, the other software you have installed. Run these update tools regularly.*

*Note that there are other ways in which your devices can be compromised at a level underneath the operating system; this cannot be remedied by an OS reinstall. If your computers have been handled by third parties you don't trust or out of your possession in a hostile environment, or if you suspect intrusion by powerful or well-resourced entities, get a new computer and call a security professional.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Minimize or eliminate the use of shared accounts where more than one person, especially less-vetted parties like volunteers, can log in to your systems using the same credentials.**  
*While in the short term it seems expedient and can be cheaper to share accounts and login information, the long-term ability to monitor and control access is more important to security outcomes. In addition, the disruption and security concerns caused by changing a broadly used password and sharing it around are potential costs that shouldn't be ignored. Sophisticated systems like G Suite or Office 365 allow for "account delegation," where two people can share an account using their own distinct login credentials; this is a better way to solve these challenges than account sharing.*

Score: ____ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Have a disaster recovery plan that includes making and testing regular backups of organizational data that are stored away from your main office site. Backup drives should at a minimum be stored in a physically secure location like a locking file cabinet or safety deposit box, and ideally encrypted so that only you can access them. Do not rely exclusively on third parties to back up and hold your information.**  
*This digital security practice is a straightforward way to protect yourself from a whole host of events that could compromise your information's integrity or cause you to lose access to it; it is so critical that it needs to come before any other digital security steps. Talk to your technical support provider about the status of your backups and when restoring data from them they was last tested. Refer to [this guide](http://www.techsoup.org/disaster-planning-and-recovery) (http://www.techsoup.org/disaster-planning-and-recovery) and/or [this webinar](http://www.communityit.com/resources/webinar-february-18-2016-backups-and-disaster-recovery-for-nonprofits/) (http://www.communityit.com/resources/webinar-february-18-2016-backups-and-disaster-recovery-for-nonprofits/) for ideas on how to improve your disaster preparedness.*

Subtotal, Baseline Capacities: ____

Total Score (add up all subtotals): ____
