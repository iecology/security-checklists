---
document set: DRAFT DIGITAL SECURITY CHECKLISTS FOR U.S. NON-PROFITS
title: Appendix A: Glossary
author: Jonah Silas Sheridan, Lisa Jervis
version: "2.0, PEER REVIEWED"
last modified: 10/27/17
---

# Appendix A: Digital Security Glossary

**Add-on**  
See "extension".

**Backup**  
Regularly updated copies of your digital assets, ideally stored in several different places, so that if access to or integrity of your data is disrupted for any reason (damage to computers due to accident or natural disaster, accidental or malicious deletion of files, etc.), the assets can be restored. Online backup services such as Mozy and CrashPlan are best supplemented by backups stored on organizational equipment with at least one up-to-date copy in secure offsite storage.  

**Cookies**  
Small files placed on your computer by websites that you visit; they are used to manage website features such as logins and can also be used to track behavior on the web. While not all cookies are a security risk, if poorly implemented they can expose the information they contain. More information about cookies is available at [http://www.allaboutcookies.org/]("http://www.allaboutcookies.org/").   

**Digital assets**  
Any and all data electronically stored or used by your organization. This includes your organization's files, website, emails, social media accounts, online banking accounts, etc. Some of these items may be ones that you administer yourself (e.g., the contents of staff hard drives, file repositories stored on servers owned and controlled by your organization); others may be maintained by third-party services on your behalf (e.g., files on Google Drive or Box). Others are services that you participate in that are owned and controlled by others (subject to terms of service), such as organizational Facebook pages.

**DKIM records**  
DomainKeys Identified Mail (DKIM) is a system to protect email from abuse, both from forged sender addresses and from content alteration. The system operates at the server level so requires help from your email provider to set up.  

**Domain Name System**  
The domain name system (DNS) is like a phone book for the Internet. It translates domain names (such as iecology.org or reddit.com) into the numbers (IP addresses) used to find services on the Internet. It can also be used to store other information about your organization's information systems, such as SPF records or DKIM keys.  

**Encryption**  
A mechanism by which your data scrambled in order to protect it from being read by unauthorized parties. Authorized parties are able to decrypt (i.e., unscramble) it. There are many different ways to encrypt communications and other digital assets.  

**Encryption key**  
An encryption key is a piece of information that you share with an authorized party so they can encrypt and/or decrypt information to or from you. In most cases this information is highly sensitive and needs to be protected; however, some modern encryption schemes (asymmetric encryption, also known as public key encryption) allow you to have a public key that you can safely share with anyone.  

**Extension**  
A small pieces of software that you install as part of your web browser in order to give your browser additional capabilities.

**Firewall**  
A piece of software or a hardware device that sits between a device or network and the Internet. It analyzes and selectively blocks or alters information passing between two sides. Common places to find firewalls are between your office network and the Internet and on your computer to protect you from other computers on your office network.

**Full disk encryption**
An encryption setup where the entirety of a storage device, or disk--whether a USB stick, hard drive inside a computer or external drive for backups or any other--is encrypted. This is the most secure way of protecting your information as unencrypted parts of disks can accidentally hold sensitive data, even if just used for "virtual memory" or you think the files on it have been deleted. This is important especially for devices that could be lost, such as laptops, mobile phones, or backup drives--but will also mean that no data on them can be accessed (including for starting up the system in the case of a computer or phone) without the encryption password.

**Office network**  
The equipment in your office that allows staff computers to connect to each other, to on-site resources such as fileservers, and to the Internet. If you cannot trust that nobody else is controlling this network your security progress will be compromised.  

**Operating system**
The main program that lets you run all the other programs on your computer. This usually includes all the tools to make your peripherals (such as keyboards or screens or storage devices) available and usually includes some sort of file manager--a way to find and access your files and programs. Common operating systems include Android, ChromeOS iOS, Linux, OSX, and Windows, but there are many others available used for all sorts of purposes.

**Password manager software**  
Software that keeps your passwords in an encrypted format, protected by a master password. This allows you to store multiple passwords by remembering only one. Password managers are available as software that you install (e.g., KeePass) and as a web-based service (e.g., LastPass). While web-based password managers can be secure enough to hold the passwords staff use to access their accounts for everyday purposes, they are not recommended to store the passwords that grant administrative access to core organizational systems.

**Security certificate**  
A security certificate is a specific kind of file that includes an encryption key and, often, additional information about that key. Websites used for banking and other sensitive services frequently use them to allow you to establish a secure connection with their servers.

**Small Message Service (SMS)**
Also known as a text message, SMS is generally an insecure way to send information to other people. It is relatively easy for those with technical equipment and knowhow to intercept cellular network traffic. In addition, many recent situations have shown that it is even easier to convince a cell company to hand over control of an account or to just steal a handset. SMS should especially be avoided as a second factor for authentication (see "two-factor authentication" below) for these reasons.

**SPF records**  
Sender Policy Framework (SPF) is a system that allows you to tell others what servers and services are allowed to send email for your organization's domain name. Setting up this record requires the assistance of your DNS provider and can have unintended negative consequences for your email delivery if not properly done.  

**Two-factor or multifactor authentication**
A way of identifying yourself to a computer or service that includes two or more items--often something you have (one-time code or specialized USB device) and something you know (like a password). Commonly one of those methods is an SMS message (see above); however, *this is no longer recommended and should be avoided due to the ease of gaining access to other people's cell service, phone, or SIM card*.   

**Virtual Private Network (VPN)**  
A connection between computers or devices that allows them to exchange information in an encrypted form. This can allow you to tunnel out of a network you don't trust or access information on your office network from someplace else on the Internet.

**Wireless Access Point**  
A wireless access point (WAP) is a piece of hardware configured to host a wireless network. In many small networks the WAP will also be a firewall separating the network from the rest of the Internet.  

**WEP, WPA, and WPA2**  
All are methods of encrypting wireless network traffic between a device like a computer or phone and a wireless access point. WEP is an older encryption method and it is far less secure than WPA and its more secure successor WPA2. Note, however, that some broad attacks on WPA2 have recently come to light as of October 2017.
