
# Introduction  

The documents in this repository comprise a set of digital security checklists for use by U.S.-based non-profit organizations with a focus on human practices and organizational management. They were created by [Information Ecology](https://iecology.org), an Oakland, California-based consultancy focusing on technology management and capacity building for progressive organizations for use in the [Weathering The Storms project of RoadMap Consulting](https://roadmapconsulting.org/resource/weathering-the-storm/). They have been peer-reviewed for readability and accuracy by both technical and operations professionals from the global non-profit community.


**These documents are designed for use in the U.S. domestic context and focused on common vulnerabilities. Use in other countries or to defend against highly aggressive and/or resourced attackers is not recommended without further threat analysis and tuning of content. [Contact us](https://iecology.org/contact) if you need support using these checklists in that way.**

**This document set is version 2.01 and was last updated in November 2017.**

# Contents

1. [Introduction](1_checklist_introduction.md)  
About these checklists and how they can help you.

2. [Readiness Assessment Tool](2_readiness_assessment_tool.md)  
A tool for assessing whether an organization has the requisite baseline capacities needed to successfully take on new digital security practices. Any challenges identified should be met before attempting to increase digital security levels through other means.

3. [Directions](3_directions_and_legend.md)  
How to use these documents and what the symbols in them mean.

4. [Device Security Checklist](4_device_security_checklist.md)  
All security depends on the ability to control your devices. This checklist helps you do that.

5. [Password and Authentication Checklist](5_authentication_checklist.md)  
A checklist of tasks related to improving the way you identify, or "authenticate," yourself to the services you use, including password management practices.

6. [Wireless Network Safety Checklist](6_wireless_checklist.md)  
A checklist of tasks related to improving security levels when depending on wireless networks.

7. [Email Safety Checklist](7_email_safety_checklist.md)  
A checklist of tasks related to safe(r) use of email.

8. [G Suite Security Checklist](8_gsuite_security_checklist.md)  
A checklist to help you set up and use the security controls in Google's domain-based services.

9. [Appendix A: Glossary](A_glossary.md)  
A glossary defining the technical terms used in these documents in as non-technical language as possible.

10. [Appendix B: Assumed Threat Model](B_threat_model.md)  
A narrative threat model describing assumed operating environment, end-user capabilities, and adversary capabilities for use by technical readers and technical support personnel.

11. [Appendix C: Frequently Asked Questions](C_FAQ.md)  
A set of questions and answers about these checklists including their origin, design, and how to provide feedback on them.

## Finally...
These documents could not exist without the support of a large group of readers, whose technical and operations peer-review and feedback tuned them, as well as the financial support of [RoadMap Consulting](https://roadmapconsulting.org), with whom we are actively using these as a tool to support our clients and communities.

**This work is dedicated to to the humans and organizations working on the front lines of important change making work everywhere.**

This content is released under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/) and can be remixed, translated, or amended freely as long as the results are shared in turn and the original documents are attributed to Information Ecology.

![Creative Commons Attribution-ShareAlike 4.0 International License Image](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

See the [license file](LICENSE) for full license terms.
